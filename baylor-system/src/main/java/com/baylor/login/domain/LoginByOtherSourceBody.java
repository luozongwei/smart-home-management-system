package com.baylor.login.domain;

/**
 * @Author Baylor Luo
 * @Date 2024/1/3 12:02
 * @Description
 */
public class LoginByOtherSourceBody {

    private String code;
    private String source;
    private String uuid;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return "LoginByOtherSourceBody{" +
                "code='" + code + '\'' +
                ", source='" + source + '\'' +
                ", uuid='" + uuid + '\'' +
                '}';
    }
}
