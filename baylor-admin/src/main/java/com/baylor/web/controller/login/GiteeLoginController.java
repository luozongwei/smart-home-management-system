package com.baylor.web.controller.login;

import com.baylor.common.constant.Constants;
import com.baylor.common.core.domain.AjaxResult;
import com.baylor.common.utils.uuid.IdUtils;
import com.baylor.framework.web.service.SysLoginService;
import com.baylor.login.domain.LoginByOtherSourceBody;
import me.zhyd.oauth.config.AuthConfig;
import me.zhyd.oauth.request.AuthGiteeRequest;
import me.zhyd.oauth.request.AuthRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Baylor Luo
 * @Date 2024/1/3 11:30
 * @Description
 */
@RestController
public class GiteeLoginController {

    @Autowired
    private SysLoginService loginService;

    @GetMapping("/PreLoginByGitee")
    public AjaxResult PreLoginByGitee() {
        AjaxResult ajax = AjaxResult.success();
        AuthRequest authRequest = new AuthGiteeRequest(AuthConfig.builder()
                .clientId("46a7b6bd83676b231eac94338808820af9bd775fcea07b857669d52b4258bd79")
                .clientSecret("ad7a055d62d528df164953b363ec73d57e4679fde8a0e0a75da1ae232634c8ad")
                .redirectUri("http://localhost/callback")
                .build());
        String uuid = IdUtils.fastUUID();
        String authorizeUrl = authRequest.authorize(uuid);
        //存储
        ajax.put("authorizeUrl", authorizeUrl);
        ajax.put("uuid", uuid);
        return ajax;
    }

    @PostMapping("/loginByGitee")
    public AjaxResult loginByGitee(@RequestBody LoginByOtherSourceBody loginByOtherSourceBody) {
        AjaxResult ajax = AjaxResult.success();
        String token = loginService
                .loginByOtherSource(loginByOtherSourceBody.getCode(), loginByOtherSourceBody.getSource(), loginByOtherSourceBody.getUuid());
        ajax.put(Constants.TOKEN, token);
        return ajax;
    }
}
