package com.baylor.web.controller.weather;

import com.baylor.common.core.domain.AjaxResult;
import com.baylor.common.utils.http.HttpUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@RestController
public class WeatherController {

    @GetMapping("/getWeatherByLocalIP")
    public AjaxResult getWeather() throws UnsupportedEncodingException {
        AjaxResult result = AjaxResult.success();
        String localCityName = GetLocationAndIP.getLocalCityName();
        // 调用天气的API
        String encodeCity = URLEncoder.encode(localCityName, "UTF-8");
        System.out.println("encodeCity = " + encodeCity);
        String url = "http://apis.juhe.cn/simpleWeather/query?city="+encodeCity+"&key=6a981daf17d64c0b443de35c347f89b5";
        String weatherInfo = HttpUtils.sendGet(url);
        result.put("msg",weatherInfo);
        return result;
    }
}